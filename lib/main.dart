import 'package:flutter/material.dart';
import 'package:testindex/app_resources/app_dimention/screen_dimension.dart';
import 'package:testindex/desktop_view.dart';
import 'package:testindex/mobile_view.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      //home: const CurveContainer(),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}



class MyHomePage extends StatelessWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);
    if (screenDimension.isMobile) {
      return const MobileView();
    }
    return  const DesktopView();
  }
}
