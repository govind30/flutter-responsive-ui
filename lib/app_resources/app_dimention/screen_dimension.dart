import 'package:flutter/material.dart';

class ScreenDimension {
  final BuildContext context;

  ScreenDimension(this.context);

  bool get isMobile {
    if (MediaQuery.of(context).size.width < 600) {
      return true;
    }
    return false;
  }

  double height(double height) {
    if(isMobile){
      return MediaQuery.of(context).size.height * (height / 805);
    }
    return MediaQuery.of(context).size.height * (height / 649);
  }

  double width(double width) {
    if(isMobile){
      return MediaQuery.of(context).size.width * (width / 400);
    }
    return MediaQuery.of(context).size.width * (width / 1366);
  }
}
