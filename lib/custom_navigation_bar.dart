import 'package:flutter/material.dart';
import 'package:testindex/app_resources/app_colors.dart';

import 'app_resources/app_dimention/screen_dimension.dart';

class CustomNavigationBar extends StatefulWidget {
  final void Function(int) onSelect;

  const CustomNavigationBar({Key? key, required this.onSelect})
      : super(key: key);

  @override
  State<CustomNavigationBar> createState() => _CustomNavigationBarState();
}

class _CustomNavigationBarState extends State<CustomNavigationBar> {
  int flag = 1;

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);

    return Container(
      height: screenDimension.height(45),
      width: screenDimension.height(480),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(12),
        border: Border.all(
            color: AppColors.lightBlue0, width: 1, style: BorderStyle.solid),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(12),
        child: Row(
          children: [
            Expanded(
              child: Container(
                height: screenDimension.height(100),
                color: flag == 1 ? AppColors.lightBlue3 : AppColors.white,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      flag = 1;
                    });
                    widget.onSelect.call(1);
                  },
                  child: Center(
                    child: Text(
                      "Arbeitnehmer",
                      style: TextStyle(
                          fontSize: screenDimension.height(14),
                          color: flag == 1
                              ? AppColors.white
                              : AppColors.lightBlue4, fontFamily: "Lato"),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: 1,
              color: AppColors.lightBlue0,
            ),
            Expanded(
              child: Container(
                color: flag == 2 ? AppColors.lightBlue3 : AppColors.white,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      flag = 2;
                    });
                    widget.onSelect.call(2);
                  },
                  child: Center(
                    child: Text(
                      "Arbeitgeber",
                      style: TextStyle(
                          fontSize: screenDimension.height(14),
                          color: flag == 2
                              ? AppColors.white
                              : AppColors.lightBlue4, fontFamily: "Lato"),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              width: 1,
              color: AppColors.lightBlue0,
            ),
            Expanded(
              child: Container(
                height: screenDimension.height(100),
                color: flag == 3 ? AppColors.lightBlue3 : AppColors.white,
                child: InkWell(
                  onTap: () {
                    setState(() {
                      flag = 3;
                    });
                    widget.onSelect.call(3);
                  },
                  child: Center(
                    child: Text(
                      "Temporärbüro",
                      style: TextStyle(
                          fontSize: screenDimension.height(14),
                          color: flag == 3
                              ? AppColors.white
                              : AppColors.lightBlue4, fontFamily: "Lato"),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
