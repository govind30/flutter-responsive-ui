import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xFF2B5A92);
  static const Color white = Color(0xFFFFFFFF);
  static const Color blue = Color(0xFF319795);
  static const Color darkBlue = Color(0xFF2B5A92);
  static const Color lightBlue1 = Color(0xFFE6FFFA);
  static const Color lightBlue1a = Color(0xFFEBF4FF);
  static const Color lightBlue1b = Color(0xFFE6FFFA);
  static const Color lightGreyv = Color(0xFFCBD5E0);


  static const Color lightBlue2 = Color(0xFF81E6D9);
  static const Color lightBlue3 = Color(0xFF81E6D9);
  static const Color lightBlue4 = Color(0xFF319795);
  static const Color lightBlue5 = Color(0xFFE6FFFA);
  static const Color darkBlue3 = Color(0xFF2D3748);
  static const Color darkBlue4 = Color(0xFF3182CE);

  static const Color darkBlue1 = Color(0xFF4A5568);
  static const Color darkGrey = Color(0xFF718096);
  static const Color white1 = Color(0xFFF7FAFC);

  static const Color lightBlue0 = Color(0xFFCBD5E0);

  static const Color lightGrey = Color(0xFFA5B4C2);
  static const Color disableColor = Color(0xFFEDF2F7);
  static const Color black = Color(0xFF333333);
  static const Color lightBlack = Color(0xFF555555);
  static const Color backgroundBlue = Color(0xFF333333);
  static const Color yellow = Color(0xFFFFF246);
  static const Color buttonBackground = Color(0xFFD9F0F8);
  static const Color homeCardBackground = Color(0xFFD9F0F8);
}
