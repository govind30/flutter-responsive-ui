import 'package:flutter/material.dart';
import 'app_resources/app_assets.dart';
import 'app_resources/app_colors.dart';
import 'app_resources/app_dimention/screen_dimension.dart';
import 'common_widgets/clippers.dart';
import 'custom_navigation_bar.dart';
import 'main_body.dart';

class DesktopView extends StatefulWidget {
  const DesktopView({Key? key}) : super(key: key);

  @override
  State<DesktopView> createState() => _DesktopViewState();
}

class _DesktopViewState extends State<DesktopView> {
  int _flag = 0;
  final ScrollController _controller = ScrollController();
  bool value = false;

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      if (_controller.position.pixels > 500) {
        setState(() {
          value = true;
        });
      } else {
        setState(() {
          value = false;
        });
      }
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);
    return Scaffold(
        backgroundColor: AppColors.white,
        body: SafeArea(
          child: Stack(
            children: [
              SingleChildScrollView(
                  controller: _controller,
                  child: Stack(
                    children: [
                      Column(
                        children: [
                          SizedBox(height: screenDimension.height(50)),
                          ClipPath(
                            clipper: BackGroundClipper2(),
                            child: Container(
                                height: screenDimension.height(380),
                                decoration: const BoxDecoration(
                                  gradient: LinearGradient(
                                      colors: [
                                        AppColors.lightBlue1a,
                                        AppColors.lightBlue1b,
                                      ],
                                      begin: FractionalOffset(0.0, 0.0),
                                      end: FractionalOffset(1.0, 0.0),
                                      stops: [0.0, 1.0],
                                      tileMode: TileMode.clamp),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        const MainSubTitle(),
                                        SizedBox(
                                            height: screenDimension.height(50)),
                                        const RegistrationButton(),
                                      ],
                                    ),
                                    SizedBox(
                                      width: screenDimension.width(50),
                                    ),
                                    Container(
                                      width: screenDimension.height(250),
                                      decoration: const BoxDecoration(
                                        color: AppColors.white,
                                        image: DecorationImage(
                                          fit: BoxFit.fitWidth,
                                          image: AssetImage(
                                            AppAssets.body4,
                                          ),
                                        ),
                                        shape: BoxShape.circle,
                                      ),
                                    ),
                                    SizedBox(width: screenDimension.width(300))
                                  ],
                                )),
                          ),
                          SizedBox(height: screenDimension.height(10)),
                          CustomNavigationBar(
                            onSelect: (int flag) {
                              setState(() {
                                _flag = flag;
                              });
                            },
                          ),
                          SizedBox(height: screenDimension.height(30)),
                          const MainTitle(),
                          MainBody1(
                            page: _flag,
                          ),
                          MainBody2(
                            page: _flag,
                          ),
                          MainBody3(
                            page: _flag,
                          )
                        ],
                      ),
                      Positioned(
                          top: screenDimension.height(950),
                          left: screenDimension.width(400),
                          child: Image.asset(
                        AppAssets.thread1,
                        height: 400,
                        width: 400,
                      )),
                      Positioned(
                        top: screenDimension.height(680),
                        left: screenDimension.width(450),
                        child: Image.asset(
                          AppAssets.thread2,
                          height: 400,
                          width: 350,
                        ),
                      )
                    ],
                  )),
              value ? const MainAppBar2() : const MainAppBar(),
            ],
          ),
        ));
  }
}
