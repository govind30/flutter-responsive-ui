
import 'package:flutter/cupertino.dart';

class BackGroundClipper2 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height);
    var firstStartPoint =
    Offset(size.width / 4 - size.width / 8, size.height - 40);
    var firstEndPoint =
    Offset(size.width / 4 + size.width / 8, size.height - 40);
    path.quadraticBezierTo(firstStartPoint.dx, firstStartPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secStartPoint = Offset(size.width / 2, size.height - 30);
    var secEndPoint = Offset(size.width / 2 + size.width / 8, size.height - 30);
    path.quadraticBezierTo(
        secStartPoint.dx, secStartPoint.dy, secEndPoint.dx, secEndPoint.dy);

    var tStartPoint =
    Offset(3 * (size.width / 4) + size.width / 8, size.height - 30);
    var tEndPoint = Offset(size.width, size.height - 90);
    path.quadraticBezierTo(
        tStartPoint.dx, tStartPoint.dy, tEndPoint.dx, tEndPoint.dy);

    //path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}

class BackGroundClipper3 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();

    path.lineTo(0, size.height);

    var firstStartPoint = Offset(size.width / 3, size.height - 20);
    var firstEndPoint = Offset(size.width - size.width / 3, size.height);
    path.quadraticBezierTo(firstStartPoint.dx, firstStartPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);
    var secStartPoint = Offset(size.width - size.width / 5, size.height);
    var secEndPoint = Offset(size.width, size.height - 40);
    path.quadraticBezierTo(
        secStartPoint.dx, secStartPoint.dy, secEndPoint.dx, secEndPoint.dy);
    path.lineTo(size.width, 50);

    var tStartPoint = Offset(size.width - size.width / 6, 20);
    var tEndPoint = Offset(size.width - size.width / 4, 20);
    path.quadraticBezierTo(
        tStartPoint.dx, tStartPoint.dy, tEndPoint.dx, tEndPoint.dy);

    var mStartPoint = Offset(size.width / 2, 40);
    var mEndPoint = Offset(size.width / 3, 20);
    path.quadraticBezierTo(
        mStartPoint.dx, mStartPoint.dy, mEndPoint.dx, mEndPoint.dy);

    // var fStartPoint = Offset(size.width-size.width/4 , 0);
    // var fEndPoint = Offset(0, 30);
    // path.quadraticBezierTo(
    //     fStartPoint.dx, fStartPoint.dy, fEndPoint.dx, fEndPoint.dy);

    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}

class BackGroundClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height);
    var firstStartPoint =
    Offset(size.width / 4 - size.width / 8, size.height - 40);
    var firstEndPoint =
    Offset(size.width / 4 + size.width / 8, size.height - 40);
    path.quadraticBezierTo(firstStartPoint.dx, firstStartPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secStartPoint = Offset(size.width / 2, size.height - 30);
    var secEndPoint = Offset(size.width / 2 + size.width / 8, size.height - 30);
    path.quadraticBezierTo(
        secStartPoint.dx, secStartPoint.dy, secEndPoint.dx, secEndPoint.dy);

    var tStartPoint =
    Offset(3 * (size.width / 4) + size.width / 8, size.height - 30);
    var tEndPoint = Offset(size.width, size.height - 90);
    path.quadraticBezierTo(
        tStartPoint.dx, tStartPoint.dy, tEndPoint.dx, tEndPoint.dy);

    //path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);

    var fStartPoint = Offset(size.width, 50);
    var fEndPoint = Offset(size.width - 100, 50);
    path.quadraticBezierTo(
        fStartPoint.dx, fStartPoint.dy, fEndPoint.dx, fEndPoint.dy);

    var ffStartPoint = Offset(size.width - 100, 50);
    var ffEndPoint = Offset(size.width / 3, 30);
    path.quadraticBezierTo(
        ffStartPoint.dx, ffStartPoint.dy, ffEndPoint.dx, ffEndPoint.dy);

    var sxStartPoint = Offset(size.width / 4, 20);
    var sxEndPoint = const Offset(0, 50);
    path.quadraticBezierTo(
        sxStartPoint.dx, sxStartPoint.dy, sxEndPoint.dx, sxEndPoint.dy);
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) {
    return true;
  }
}
