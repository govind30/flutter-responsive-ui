import 'package:flutter/material.dart';
import 'package:testindex/app_resources/app_assets.dart';
import 'package:testindex/app_resources/app_colors.dart';
import 'package:testindex/app_resources/app_dimention/screen_dimension.dart';
import 'common_widgets/clippers.dart';

class MainBody1 extends StatelessWidget {
  final int page;

  MainBody1({Key? key, required this.page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String bodyText = page == 1
        ? "Erstellen dein Lebenslauf"
        : "Erstellen dein Unternehmensprofil";

    ScreenDimension screenDimension = ScreenDimension(context);
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: screenDimension.height(100),
              width: screenDimension.height(360),
              child: Stack(
                children: [
                  Container(
                    height: screenDimension.height(100),
                    width: screenDimension.height(100),
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: AppColors.white1,
                    ),
                    child: Center(
                      child: Text(
                        "1.",
                        style: TextStyle(
                            color: AppColors.darkGrey,
                            fontSize: screenDimension.height(60),
                            fontFamily: "Lato"),
                      ),
                    ),
                  ),
                  Positioned(
                      top: screenDimension.height(50),
                      left: screenDimension.height(75),
                      child: Text(
                        bodyText,
                        style: TextStyle(
                            color: AppColors.darkGrey,
                            fontSize: 18,
                            fontFamily: "Lato"),
                      ))
                ],
              ),
            ),
            Image.asset(
              AppAssets.body1,
              height: screenDimension.height(300),
              width: screenDimension.width(300),
            )
          ],
        )
      ],
    );
  }
}

class MainBodyM1 extends StatelessWidget {
  final int page;

  const MainBodyM1({Key? key, required this.page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);
    String bodyText = page == 1
        ? "Erstellen dein Lebenslauf"
        : "Erstellen dein\nUnternehmensprofil";
    return SizedBox(
      height: screenDimension.height(250),
      // width: screenDimension.height(350),
      child: Stack(
        children: [
          Positioned(
            bottom: 0,
            left: -screenDimension.width(40),
            child: Container(
              height: screenDimension.height(220),
              width: screenDimension.height(220),
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: AppColors.white1,
              ),
              child: Center(
                child: Text(
                  "1.",
                  style: TextStyle(
                      color: AppColors.darkGrey,
                      fontSize: screenDimension.height(120),
                      fontFamily: "Lato"),
                ),
              ),
            ),
          ),
          Positioned(
              bottom: screenDimension.height(60),
              left: screenDimension.height(120),
              child: Text(
                bodyText,
                style: TextStyle(
                    color: AppColors.darkGrey,
                    fontSize: screenDimension.height(20),
                    fontFamily: "Lato"),
              )),
          Positioned(
            top: screenDimension.height(-80),
            left: screenDimension.height(100),
            child: Image.asset(
              AppAssets.body1,
              height: screenDimension.height(300),
              width: screenDimension.height(200),
            ),
          ),
        ],
      ),
    );
  }
}

class MainBody2 extends StatelessWidget {
  final int page;

  MainBody2({Key? key, required this.page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String bodyText = page == 1
        ? "Erstellen dein Lebenslauf"
        : page == 2
            ? "Erstellen ein Jobinserat"
            : "Erhalte Vermittlungs- \nangebot von Arbeitgeber";
    String image = page == 1
        ? AppAssets.body3
        : page == 2
            ? AppAssets.body5
            : AppAssets.body7;
    ScreenDimension screenDimension = ScreenDimension(context);
    return ClipPath(
      clipper: BackGroundClipper3(),
      child: Container(
        height: screenDimension.height(300),
        decoration: const BoxDecoration(
            gradient: LinearGradient(
                colors: [
                  AppColors.lightBlue1b,
                  AppColors.lightBlue1a,
                ],
                begin: FractionalOffset(0.0, 0.0),
                end: FractionalOffset(1.0, 0.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          // crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Image.asset(
              image,
              height: screenDimension.height(300),
              width: screenDimension.width(300),
            ),
            SizedBox(
              width: screenDimension.width(50),
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                "2.",
                style: TextStyle(
                    color: AppColors.darkGrey,
                    fontSize: screenDimension.height(60),
                    fontFamily: "Lato"),
              ),
            ),
            Align(
              alignment: Alignment.center,
              child: Text(
                bodyText,
                style: const TextStyle(
                    color: AppColors.darkGrey,
                    fontSize: 18,
                    fontFamily: "Lato"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MainBodyM2 extends StatelessWidget {
  final int page;

  const MainBodyM2({Key? key, required this.page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String bodyText = page == 1
        ? "Erstellen dein Lebenslauf"
        : page == 2
            ? "Erstellen ein Jobinserat"
            : "Erhalte Vermittlungs- \nangebot von Arbeitgeber";
    String image = page == 1
        ? AppAssets.body3
        : page == 2
            ? AppAssets.body5
            : AppAssets.body7;
    ScreenDimension screenDimension = ScreenDimension(context);
    return ClipPath(
      clipper: BackGroundClipper(),
      child: Container(
          height: screenDimension.height(400),
          decoration: const BoxDecoration(
              gradient: LinearGradient(
                  colors: [
                    AppColors.lightBlue1a,
                    AppColors.lightBlue1b,
                  ],
                  begin: FractionalOffset(0.0, 0.0),
                  end: FractionalOffset(1.0, 0.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "2.",
                    style: TextStyle(
                        color: AppColors.darkGrey,
                        fontSize: screenDimension.height(120),
                        fontFamily: "Lato"),
                  ),
                  Column(
                    children: [
                      Text(
                        bodyText,
                        style: TextStyle(
                            color: AppColors.darkGrey,
                            fontSize: 18,
                            fontFamily: "Lato"),
                      ),
                      SizedBox(
                        height: screenDimension.height(20),
                      )
                    ],
                  )
                ],
              ),
              Image.asset(
                image,
                height: screenDimension.height(200),
                width: screenDimension.height(200),
              ),
            ],
          )),
    );
  }
}

class MainBody3 extends StatelessWidget {
  final int page;

  const MainBody3({Key? key, required this.page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String bodyText = page == 1
        ? "Mit nur einem Klick \nbewerben"
        : page == 2
            ? "Wähle deinen\n neuen Mitarbeiter aus"
            : "Vermittlung nach \nProvision oder Stundenlohn";
    String image = page == 1
        ? AppAssets.body2
        : page == 2
            ? AppAssets.body6
            : AppAssets.body8;
    ScreenDimension screenDimension = ScreenDimension(context);
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: screenDimension.height(200),
          width: screenDimension.width(650),
          child: Stack(
            children: [
              Container(
                height: screenDimension.height(300),
                width: screenDimension.width(300),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppColors.white1,
                ),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    "3.",
                    style: TextStyle(
                        color: AppColors.darkGrey,
                        fontSize: screenDimension.height(60),
                        fontFamily: "Lato"),
                  ),
                ),
              ),
              Positioned(
                  top: screenDimension.height(10),
                  left: screenDimension.width(220),
                  child: Text(
                    bodyText,
                    style: const TextStyle(
                        color: AppColors.darkGrey,
                        fontSize: 18,
                        fontFamily: "Lato"),
                  ))
            ],
          ),
        ),
        Image.asset(
          image,
          height: screenDimension.height(300),
          width: screenDimension.width(400),
        )
      ],
    );
  }
}

class MainBodyM3 extends StatelessWidget {
  final int page;

  const MainBodyM3({Key? key, required this.page}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String bodyText = page == 1
        ? "Mit nur einem Klick \nbewerben"
        : page == 2
            ? "Wähle deinen\n neuen Mitarbeiter aus"
            : "Vermittlung nach \nProvision oder \nStundenlohn";
    String image = page == 1
        ? AppAssets.body2
        : page == 2
            ? AppAssets.body6
            : AppAssets.body8;
    ScreenDimension screenDimension = ScreenDimension(context);
    return SizedBox(
        height: screenDimension.height(350),
        child: Stack(
          children: [
            Positioned(
              top: 0,
              left: screenDimension.height(-50),
              child: Container(
                height: screenDimension.height(300),
                width: screenDimension.height(300),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: AppColors.white1,
                ),
                child: Align(
                  alignment: Alignment.topCenter,
                  child: Text(
                    "3.",
                    style: TextStyle(
                        fontFamily: "Lato",
                        color: AppColors.darkGrey,
                        fontSize: screenDimension.height(120)),
                  ),
                ),
              ),
            ),
            Positioned(
                top: screenDimension.height(60),
                left: screenDimension.height(180),
                child: Text(
                  bodyText,
                  style: TextStyle(
                      color: AppColors.darkGrey,
                      fontFamily: "Lato",
                      fontSize: screenDimension.height(18)),
                )),
            Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset(
                image,
                height: screenDimension.height(220),
                width: screenDimension.height(220),
              ),
            ),
          ],
        ));
  }
}

class MainTitle extends StatelessWidget {
  const MainTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);

    return Text(
      "Drei einfache Schritte \nzu deinem neuen Job",
      style: TextStyle(
          color: AppColors.darkBlue1,
          fontFamily: "Lato",
          fontSize: screenDimension.height(screenDimension.height(25))),
    );
  }
}

class MainSubTitle extends StatelessWidget {
  const MainSubTitle({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);

    return Text(
      "Deine Job\nwebsite",
      style: TextStyle(
          fontFamily: "Lato",
          color: AppColors.darkBlue3,
          fontSize: screenDimension.height(45)),
      textAlign: screenDimension.isMobile ? TextAlign.center : TextAlign.start,
    );
  }
}

class RegistrationButton extends StatelessWidget {
  const RegistrationButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);

    return Container(
      height: screenDimension.height(30),
      width: screenDimension.height(200),
      decoration: BoxDecoration(
        color: AppColors.darkBlue4,
        gradient: const LinearGradient(
            colors: [
              Color(0xFF319795),
              Color(0xFF3182CE),
            ],
            begin: FractionalOffset(0.0, 0.0),
            end: FractionalOffset(1.0, 0.0),
            stops: [0.0, 1.0],
            tileMode: TileMode.clamp),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Center(
        child: Text(
          "Kostenlos Registrieren",
          style: TextStyle(
              color: AppColors.white,
              fontSize: screenDimension.height(15),
              fontFamily: "Lato"),
        ),
      ),
    );
  }
}

class RegistrationButton2 extends StatelessWidget {
  const RegistrationButton2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);
    return Container(
      height: screenDimension.height(30),
      width: 250,
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.lightGreyv),
        borderRadius: BorderRadius.circular(10),
      ),
      child: const Center(
        child: Text(
          "Kostenlos Registrieren",
          style: TextStyle(
              color: AppColors.blue, fontSize: 14, fontFamily: "Lato"),
        ),
      ),
    );
  }
}

class MainBottomBar extends StatelessWidget {
  const MainBottomBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);

    return Row(
      children: [
        Expanded(
          child: Container(
            height: screenDimension.height(90),
            padding: EdgeInsets.symmetric(
                horizontal: screenDimension.height(25),
                vertical: screenDimension.height(25)),
            decoration: const BoxDecoration(
                boxShadow: [BoxShadow(blurRadius: 3, color: Colors.black54)],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(12),
                    topRight: Radius.circular(12))),
            child: const RegistrationButton(),
          ),
        )
      ],
    );
  }
}

class MainAppBar extends StatelessWidget {
  const MainAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);

    return Column(
      children: [
        Container(
          height: 5,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
            colors: [
              Color(0xFF319795),
              Color(0xFF3182CE),
            ],
          )),
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                height: screenDimension.height(67),
                decoration: const BoxDecoration(
                    boxShadow: [
                      BoxShadow(blurRadius: 3, color: Colors.black54)
                    ],
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(12),
                        bottomRight: Radius.circular(12))),
                child: Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: InkWell(
                    child: const Align(
                        alignment: Alignment.centerRight,
                        child: Text(
                          "Login",
                          style: TextStyle(
                              color: AppColors.blue, fontFamily: "Lato"),
                        )),
                    onTap: () {},
                  ),
                ),
              ),
            )
          ],
        ),
      ],
    );
  }
}

class MainAppBar2 extends StatelessWidget {
  const MainAppBar2({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);

    return Column(
      children: [
        Container(
          height: 5,
          decoration: const BoxDecoration(
              gradient: LinearGradient(
            colors: [
              Color(0xFF319795),
              Color(0xFF3182CE),
            ],
          )),
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                  height: screenDimension.height(67),
                  decoration: const BoxDecoration(
                      boxShadow: [
                        BoxShadow(blurRadius: 3, color: Colors.black54)
                      ],
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(12),
                          bottomRight: Radius.circular(12))),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      const Text(
                        "Jetzt Klicken",
                        style:
                            TextStyle(color: AppColors.darkGrey, fontSize: 19),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      RegistrationButton2(),
                      Padding(
                        padding: const EdgeInsets.all(20.0),
                        child: InkWell(
                          child: const Align(
                              alignment: Alignment.centerRight,
                              child: Text(
                                "Login",
                                style: TextStyle(
                                    color: AppColors.blue, fontFamily: "Lato"),
                              )),
                          onTap: () {},
                        ),
                      ),
                    ],
                  )),
            )
          ],
        ),
      ],
    );
  }
}
