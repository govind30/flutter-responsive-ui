import 'package:flutter/material.dart';
import 'app_resources/app_assets.dart';
import 'app_resources/app_colors.dart';
import 'app_resources/app_dimention/screen_dimension.dart';
import 'common_widgets/clippers.dart';
import 'custom_navigation_bar.dart';
import 'main_body.dart';

class MobileView extends StatefulWidget {
  const MobileView({Key? key}) : super(key: key);

  @override
  State<MobileView> createState() => _MobileViewState();
}

class _MobileViewState extends State<MobileView> {
  int _flag = 0;

  @override
  Widget build(BuildContext context) {
    ScreenDimension screenDimension = ScreenDimension(context);

    return Scaffold(
        body: SafeArea(
      child: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                ClipPath(
                  clipper: BackGroundClipper2(),
                  child: Container(
                    padding: EdgeInsets.symmetric(
                      vertical: screenDimension.height(80),
                    ),
                    decoration: const BoxDecoration(
                        gradient: LinearGradient(
                            colors: [
                              AppColors.lightBlue1a,
                              AppColors.lightBlue1b,
                            ],
                            begin: FractionalOffset(0.0, 1.0),
                            end: FractionalOffset(0.0, 0.0),
                            stops: [0.0, 1.0],
                            tileMode: TileMode.clamp)),
                    child: Column(
                      children: [
                        const MainSubTitle(),
                        Image.asset(AppAssets.body4),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: screenDimension.height(50),
                ),
                SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    child: Row(
                      children: [
                        const SizedBox(
                          width: 10,
                        ),
                        CustomNavigationBar(
                          onSelect: (int flag) {
                            setState(() {
                              _flag = flag;
                            });
                          },
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                      ],
                    )),
                SizedBox(height: screenDimension.height(50)),
                const MainTitle(),
                SizedBox(height: screenDimension.height(50)),
                SizedBox(
                  height: screenDimension.height(930),
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: MainBodyM2(
                          page: _flag,
                        ),
                      ),
                      Align(
                        alignment: Alignment.bottomCenter,
                        child: MainBodyM3(
                          page: _flag,
                        ),
                      ),
                      Align(
                        alignment: Alignment.topCenter,
                        child: MainBodyM1(
                          page: _flag,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: screenDimension.height(100))
              ],
            ),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: const [MainAppBar(), MainBottomBar()],
          )
        ],
      ),
    ));
  }
}
